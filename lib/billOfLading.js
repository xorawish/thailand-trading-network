/**
 * @param {org.thailand.trade.carrier.CreateBillOfLading} tradeData
 * @transaction
 */

async function createBillOfLading(tradeData) {
    const factory = getFactory();
    const namespace = 'org.thailand.trade.carrier';
    const billOfLadingNo = Math.floor(Math.random() * Math.floor(100000)).toString();

    const billOfLading = factory.newResource(namespace, 'BillOfLading', billOfLadingNo);
    billOfLading.shipper = tradeData.shipper;
    billOfLading.consignee = tradeData.consignee;
    billOfLading.notifyParty = tradeData.notifyParty;
    billOfLading.vessel = tradeData.vessel;
    billOfLading.portOfLoading = tradeData.portOfLoading;
    billOfLading.placeOfAcceptance = tradeData.placeOfAcceptance;
    billOfLading.numberOfOriginalBillOfLading = tradeData.numberOfOriginalBillOfLading;
    billOfLading.portOfDischarge = tradeData.portOfDischarge;
    billOfLading.finalDestination = tradeData.finalDestination;
    billOfLading.placeOfDelivery = tradeData.placeOfDelivery;
    billOfLading.freightAndChargesPayableAt = tradeData.freightAndChargesPayableAt;
    billOfLading.marksAndNumbers = tradeData.marksAndNumbers;
    billOfLading.numberAndTypeOfPackages = tradeData.numberAndTypeOfPackages;
    billOfLading.descriptionOfGoods = tradeData.descriptionOfGoods;
    billOfLading.weightKgs = tradeData.weightKgs;
    billOfLading.measurementM3 = tradeData.measurementM3;
    billOfLading.containerNos = tradeData.containerNos;
    billOfLading.typeOfService = tradeData.typeOfService;
    billOfLading.deliveryAgent = tradeData.deliveryAgent;
    billOfLading.shipDate = tradeData.shipDate;

    const assetRegistry = await getAssetRegistry(billOfLading.getFullyQualifiedType());
    await assetRegistry.add(billOfLading);

    const event = factory.newEvent(namespace, 'BillOfLadingCreated');
    event.billOfLadingNo = tradeData.billOfLadingNo;
}
