/* global getFactory getAssetRegistry getParticipantRegistry emit */

/**
 * Create the LOC asset
 * @param {org.thailand.trade.bank.InitialApplication} initalAppliation - the InitialApplication transaction
 * @transaction
 */
async function initialApplication(application) { 
    const factory = getFactory();
    const namespace = 'org.thailand.trade.bank';
    const participantsNamespace = 'org.thailand.trade.participants'

    const letter = factory.newResource(namespace, 'LetterOfCredit', application.letterId);
    letter.applicant = factory.newRelationship(participantsNamespace, 'Customer', application.applicant.getIdentifier());
    letter.beneficiary = factory.newRelationship(participantsNamespace, 'Customer', application.beneficiary.getIdentifier());
    letter.issuingBank = factory.newRelationship(participantsNamespace, 'Bank', application.applicant.bank.getIdentifier());
    letter.exportingBank = factory.newRelationship(participantsNamespace, 'Bank', application.beneficiary.bank.getIdentifier());
    letter.rules = application.rules;
    letter.productDetails = application.productDetails;
    letter.evidence = [];
    letter.approval = [factory.newRelationship(participantsNamespace, 'Customer', application.applicant.getIdentifier())];
    letter.status = 'AWAITING_APPROVAL';

    const assetRegistry = await getAssetRegistry(letter.getFullyQualifiedType());
    await assetRegistry.add(letter);

    const applicationEvent = factory.newEvent(namespace, 'InitialApplicationEvent');
    applicationEvent.loc = letter;
    emit(applicationEvent);
}

/**
 * Update the LOC to show that it has been approved by a given person
 * @param {org.thailand.trade.bank.Approve} approve - the Approve transaction
 * @transaction
 */
async function approve(approveRequest) { 
    const factory = getFactory();
    const namespace = 'org.thailand.trade.bank';
    const participantsNamespace = 'org.thailand.trade.participants'

    let letter = approveRequest.loc;

    if (letter.status === 'CLOSED' || letter.status === 'REJECTED') {
        throw new Error ('This letter of credit has already been closed');
    } else if (letter.approval.length === 4) {
        throw new Error ('All four parties have already approved this letter of credit');
    } else if (letter.approval.includes(approveRequest.approvingParty)) {
        throw new Error ('This person has already approved this letter of credit');
    } else if (approveRequest.approvingParty.getType() === 'BankEmployee') {
        letter.approval.forEach((approvingParty) => {
            let bankApproved = false;
            try {
                bankApproved = approvingParty.getType() === 'BankEmployee' && approvingParty.bank.getIdentifier() === approveRequest.approvingParty.bank.getIdentifier();
            } catch (err) {
            }
            if (bankApproved) {
                throw new Error('Your bank has already approved of this request');
            }
        });
    }

    letter.approval.push(factory.newRelationship(participantsNamespace, approveRequest.approvingParty.getType(), approveRequest.approvingParty.getIdentifier()));
    if (letter.approval.length === 4) {
        letter.status = 'APPROVED';
    }

    const assetRegistry = await getAssetRegistry(approveRequest.loc.getFullyQualifiedType());
    await assetRegistry.update(letter);

    const approveEvent = factory.newEvent(namespace, 'ApproveEvent');
    approveEvent.loc = approveRequest.loc;
    approveEvent.approvingParty = approveRequest.approvingParty;
    emit(approveEvent);
}

/**
 * Reject the LOC
 * @param {org.thailand.trade.bank.Reject} reject - the Reject transaction
 * @transaction
 */
async function reject(rejectRequest) { 
    const factory = getFactory();
    const namespace = 'org.thailand.trade.bank';

    let letter = rejectRequest.loc;

    if (letter.status === 'CLOSED' || letter.status === 'REJECTED') {
        throw new Error('This letter of credit has already been closed');
    } else if (letter.status === 'APPROVED') {
        throw new Error('This letter of credit has already been approved');
    } else {
        letter.status = 'REJECTED';
        letter.closeReason = rejectRequest.closeReason;

        const assetRegistry = await getAssetRegistry(rejectRequest.loc.getFullyQualifiedType());
        await assetRegistry.update(letter);

        const rejectEvent = factory.newEvent(namespace, 'RejectEvent');
        rejectEvent.loc = rejectRequest.loc;
        rejectEvent.closeReason = rejectRequest.closeReason;
        emit(rejectEvent);
    }
}

/**
 * Suggest changes to the current rules in the LOC
 * @param {org.thailand.trade.bank.SuggestChanges} suggestChanges - the SuggestChanges transaction
 * @transaction
 */
async function suggestChanges(changeRequest) { 
    const factory = getFactory();
    const namespace = 'org.thailand.trade.bank';

    let letter = changeRequest.loc;

    if (letter.status === 'CLOSED' || letter.status === 'REJECTED') {
        throw new Error ('This letter of credit has already been closed');
    } else if (letter.status === 'APPROVED') {
        throw new Error('This letter of credit has already been approved');
    } else if (letter.status === 'SHIPPED' || letter.status === 'RECEIVED' || letter.status === 'READY_FOR_PAYMENT') {
        throw new Error ('The product has already been shipped');
    } else {
        letter.rules = changeRequest.rules;
        letter.approval = [changeRequest.suggestingParty];
        letter.status = 'AWAITING_APPROVAL';

        const assetRegistry = await getAssetRegistry(changeRequest.loc.getFullyQualifiedType());
        await assetRegistry.update(letter);

        const changeEvent = factory.newEvent(namespace, 'SuggestChangesEvent');
        changeEvent.loc = changeRequest.loc;
        changeEvent.rules = changeRequest.rules;
        changeEvent.suggestingParty = changeRequest.suggestingParty;
        emit(changeEvent);
    }
}

/**
 * "Ship" the product
 * @param {org.thailand.trade.bank.ShipProduct} shipProduct - the ShipProduct transaction
 * @transaction
 */
async function shipProduct(shipRequest) { // eslint-disable-line no-unused-vars
    const factory = getFactory();
    const namespace = 'org.thailand.trade.bank';

    let letter = shipRequest.loc;

    if (letter.status === 'APPROVED') {
        letter.status = 'SHIPPED';
        letter.evidence.push(shipRequest.evidence);

        const assetRegistry = await getAssetRegistry(shipRequest.loc.getFullyQualifiedType());
        await assetRegistry.update(letter);

        const shipEvent = factory.newEvent(namespace, 'ShipProductEvent');
        shipEvent.loc = shipRequest.loc;
        emit(shipEvent);
    } else if (letter.status === 'AWAITING_APPROVAL') {
        throw new Error ('This letter needs to be fully approved before the product can be shipped');
    } else if (letter.status === 'CLOSED' || letter.status === 'REJECTED') {
        throw new Error ('This letter of credit has already been closed');
    } else {
        throw new Error ('The product has already been shipped');
    }
}

/**
 * "Recieve" the product that has been "shipped"
 * @param {org.thailand.trade.bank.ReceiveProduct} receiveProduct - the ReceiveProduct transaction
 * @transaction
 */
async function receiveProduct(receiveRequest) { 
    const factory = getFactory();
    const namespace = 'org.thailand.trade.bank';

    let letter = receiveRequest.loc;

    if (letter.status === 'SHIPPED') {
        letter.status = 'RECEIVED';

        const assetRegistry = await getAssetRegistry(receiveRequest.loc.getFullyQualifiedType());
        await assetRegistry.update(letter);

        const receiveEvent = factory.newEvent(namespace, 'ReceiveProductEvent');
        receiveEvent.loc = receiveRequest.loc;
        emit(receiveEvent);
    } else if (letter.status === 'AWAITING_APPROVAL' || letter.status === 'APPROVED'){
        throw new Error('The product needs to be shipped before it can be received');
    } else if (letter.status === 'CLOSED' || letter.status === 'REJECTED') {
        throw new Error ('This letter of credit has already been closed');
    } else {
        throw new Error('The product has already been received');
    }
}


/**
 * Mark a given letter as "ready for payment"
 * @param {org.thailand.trade.bank.ReadyForPayment} readyForPayment - the ReadyForPayment transaction
 * @transaction
 */
async function readyForPayment(paymentRequest) { 
    const factory = getFactory();
    const namespace = 'org.thailand.trade.bank';

    let letter = paymentRequest.loc;

    if (letter.status === 'RECEIVED') {
        letter.status = 'READY_FOR_PAYMENT';

        const assetRegistry = await getAssetRegistry(paymentRequest.loc.getFullyQualifiedType());
        await assetRegistry.update(letter);

        const paymentEvent = factory.newEvent(namespace, 'ReadyForPaymentEvent');
        paymentEvent.loc = paymentRequest.loc;
        emit(paymentEvent);
    } else if (letter.status === 'CLOSED' || letter.status === 'REJECTED') {
        throw new Error('This letter of credit has already been closed');
    } else if (letter.status === 'READY_FOR_PAYMENT') {
        throw new Error('The payment has already been made');
    } else {
        throw new Error('The payment cannot be made until the product has been received by the applicant');
    }
}

/**
 * Close the LOC
 * @param {org.thailand.trade.bank.Close} close - the Close transaction
 * @transaction
 */
async function close(closeRequest) { 
    const factory = getFactory();
    const namespace = 'org.thailand.trade.bank';

    let letter = closeRequest.loc;

    if (letter.status === 'READY_FOR_PAYMENT') {
        letter.status = 'CLOSED';
        letter.closeReason = closeRequest.closeReason;

        const assetRegistry = await getAssetRegistry(closeRequest.loc.getFullyQualifiedType());
        await assetRegistry.update(letter);

        const closeEvent = factory.newEvent(namespace, 'CloseEvent');
        closeEvent.loc = closeRequest.loc;
        closeEvent.closeReason = closeRequest.closeReason;
        emit(closeEvent);
    } else if (letter.status === 'CLOSED' || letter.status === 'REJECTED') {
        throw new Error('This letter of credit has already been closed');
    } else {
        throw new Error('Cannot close this letter of credit until it is fully approved and the product has been received by the applicant');
    }
}

/**
 * Create the participants needed for the demo
 * @param {org.thailand.trade.participants.CreateDemoParticipants} createDemoParticipants - the CreateDemoParticipants transaction
 * @transaction
 */
async function createDemoParticipants() { 
    const factory = getFactory();
    const namespace = 'org.thailand.trade.participants';

    const bankRegistry = await getParticipantRegistry(namespace + '.Bank');
    const bank1 = factory.newResource(namespace, 'Bank', 'BoD');
    bank1.name = 'Bank of Dinero';
    await bankRegistry.add(bank1);
    const bank2 = factory.newResource(namespace, 'Bank', 'EB');
    bank2.name = 'Eastwood Banking';
    await bankRegistry.add(bank2);

    const employeeRegistry = await getParticipantRegistry(namespace + '.BankEmployee');
    const employee1 = factory.newResource(namespace, 'BankEmployee', 'matias');
    employee1.name = 'Matías';
    employee1.bank = factory.newRelationship(namespace, 'Bank', 'BoD');
    await employeeRegistry.add(employee1);
    const employee2 = factory.newResource(namespace, 'BankEmployee', 'ella');
    employee2.name = 'Ella';
    employee2.bank = factory.newRelationship(namespace, 'Bank', 'EB');
    await employeeRegistry.add(employee2);

    const customerRegistry = await getParticipantRegistry(namespace + '.Customer');
    const customer1 = factory.newResource(namespace, 'Customer', 'alice');
    customer1.name = 'Alice';
    customer1.lastName= 'Hamilton';
    customer1.bank = factory.newRelationship(namespace, 'Bank', 'BoD');
    customer1.companyName = 'QuickFix IT';
    await customerRegistry.add(customer1);
    const customer2 = factory.newResource(namespace, 'Customer', 'bob');
    customer2.name = 'Bob';
    customer2.lastName= 'Appleton';
    customer2.bank = factory.newRelationship(namespace, 'Bank', 'EB');
    customer2.companyName = 'Conga Computers';
    await customerRegistry.add(customer2);
}