/**
 * @param {org.thailand.trade.dft.CreateRiceExportCertificate} data
 * @transaction
 */

async function createRiceExportCertificate(data) {
    const factory = getFactory();
    const namespace = 'org.thailand.trade.dft';
    const certificateId = Math.floor(Math.random() * Math.floor(100000)).toString();

    const riceExportCertificate = factory.newResource(namespace, 'RiceExportCertificate', certificateId);
    riceExportCertificate.person = data.person
    riceExportCertificate.companyName = data.companyName
    riceExportCertificate.contact = data.contact
    riceExportCertificate.address = data.address
    riceExportCertificate.riceType = data.riceType
    riceExportCertificate.riceWeight = data.riceWeight

    const assetRegistry = await getAssetRegistry(riceExportCertificate.getFullyQualifiedType());
    await assetRegistry.add(riceExportCertificate);

    const event = factory.newEvent(namespace, 'RiceExportCertificateCreated');
    event.certificateId = data.certificateId;
}