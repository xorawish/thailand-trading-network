/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { AngularTestPage } from './app.po';
import { ExpectedConditions, browser, element, by } from 'protractor';
import {} from 'jasmine';


describe('Starting tests for thailand-trading-network-app', function() {
  let page: AngularTestPage;

  beforeEach(() => {
    page = new AngularTestPage();
  });

  it('website title should be thailand-trading-network-app', () => {
    page.navigateTo('/');
    return browser.getTitle().then((result)=>{
      expect(result).toBe('thailand-trading-network-app');
    })
  });

  it('network-name should be thailand-trading-network@0.1.5',() => {
    element(by.css('.network-name')).getWebElement()
    .then((webElement) => {
      return webElement.getText();
    })
    .then((txt) => {
      expect(txt).toBe('thailand-trading-network@0.1.5.bna');
    });
  });

  it('navbar-brand should be thailand-trading-network-app',() => {
    element(by.css('.navbar-brand')).getWebElement()
    .then((webElement) => {
      return webElement.getText();
    })
    .then((txt) => {
      expect(txt).toBe('thailand-trading-network-app');
    });
  });

  
    it('LetterOfCredit component should be loadable',() => {
      page.navigateTo('/LetterOfCredit');
      browser.findElement(by.id('assetName'))
      .then((assetName) => {
        return assetName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('LetterOfCredit');
      });
    });

    it('LetterOfCredit table should have 12 columns',() => {
      page.navigateTo('/LetterOfCredit');
      element.all(by.css('.thead-cols th')).then(function(arr) {
        expect(arr.length).toEqual(12); // Addition of 1 for 'Action' column
      });
    });
  
    it('BillOfLading component should be loadable',() => {
      page.navigateTo('/BillOfLading');
      browser.findElement(by.id('assetName'))
      .then((assetName) => {
        return assetName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('BillOfLading');
      });
    });

    it('BillOfLading table should have 22 columns',() => {
      page.navigateTo('/BillOfLading');
      element.all(by.css('.thead-cols th')).then(function(arr) {
        expect(arr.length).toEqual(22); // Addition of 1 for 'Action' column
      });
    });
  
    it('RiceExportCertificate component should be loadable',() => {
      page.navigateTo('/RiceExportCertificate');
      browser.findElement(by.id('assetName'))
      .then((assetName) => {
        return assetName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('RiceExportCertificate');
      });
    });

    it('RiceExportCertificate table should have 8 columns',() => {
      page.navigateTo('/RiceExportCertificate');
      element.all(by.css('.thead-cols th')).then(function(arr) {
        expect(arr.length).toEqual(8); // Addition of 1 for 'Action' column
      });
    });
  

  
    it('Carrier component should be loadable',() => {
      page.navigateTo('/Carrier');
      browser.findElement(by.id('participantName'))
      .then((participantName) => {
        return participantName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('Carrier');
      });
    });

    it('Carrier table should have 2 columns',() => {
      page.navigateTo('/Carrier');
      element.all(by.css('.thead-cols th')).then(function(arr) {
        expect(arr.length).toEqual(2); // Addition of 1 for 'Action' column
      });
    });
  
    it('Bank component should be loadable',() => {
      page.navigateTo('/Bank');
      browser.findElement(by.id('participantName'))
      .then((participantName) => {
        return participantName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('Bank');
      });
    });

    it('Bank table should have 3 columns',() => {
      page.navigateTo('/Bank');
      element.all(by.css('.thead-cols th')).then(function(arr) {
        expect(arr.length).toEqual(3); // Addition of 1 for 'Action' column
      });
    });
  
    it('Customer component should be loadable',() => {
      page.navigateTo('/Customer');
      browser.findElement(by.id('participantName'))
      .then((participantName) => {
        return participantName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('Customer');
      });
    });

    it('Customer table should have 6 columns',() => {
      page.navigateTo('/Customer');
      element.all(by.css('.thead-cols th')).then(function(arr) {
        expect(arr.length).toEqual(6); // Addition of 1 for 'Action' column
      });
    });
  
    it('BankEmployee component should be loadable',() => {
      page.navigateTo('/BankEmployee');
      browser.findElement(by.id('participantName'))
      .then((participantName) => {
        return participantName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('BankEmployee');
      });
    });

    it('BankEmployee table should have 5 columns',() => {
      page.navigateTo('/BankEmployee');
      element.all(by.css('.thead-cols th')).then(function(arr) {
        expect(arr.length).toEqual(5); // Addition of 1 for 'Action' column
      });
    });
  

  
    it('InitialApplication component should be loadable',() => {
      page.navigateTo('/InitialApplication');
      browser.findElement(by.id('transactionName'))
      .then((transactionName) => {
        return transactionName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('InitialApplication');
      });
    });
  
    it('Approve component should be loadable',() => {
      page.navigateTo('/Approve');
      browser.findElement(by.id('transactionName'))
      .then((transactionName) => {
        return transactionName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('Approve');
      });
    });
  
    it('Reject component should be loadable',() => {
      page.navigateTo('/Reject');
      browser.findElement(by.id('transactionName'))
      .then((transactionName) => {
        return transactionName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('Reject');
      });
    });
  
    it('SuggestChanges component should be loadable',() => {
      page.navigateTo('/SuggestChanges');
      browser.findElement(by.id('transactionName'))
      .then((transactionName) => {
        return transactionName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('SuggestChanges');
      });
    });
  
    it('ShipProduct component should be loadable',() => {
      page.navigateTo('/ShipProduct');
      browser.findElement(by.id('transactionName'))
      .then((transactionName) => {
        return transactionName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('ShipProduct');
      });
    });
  
    it('ReceiveProduct component should be loadable',() => {
      page.navigateTo('/ReceiveProduct');
      browser.findElement(by.id('transactionName'))
      .then((transactionName) => {
        return transactionName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('ReceiveProduct');
      });
    });
  
    it('ReadyForPayment component should be loadable',() => {
      page.navigateTo('/ReadyForPayment');
      browser.findElement(by.id('transactionName'))
      .then((transactionName) => {
        return transactionName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('ReadyForPayment');
      });
    });
  
    it('Close component should be loadable',() => {
      page.navigateTo('/Close');
      browser.findElement(by.id('transactionName'))
      .then((transactionName) => {
        return transactionName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('Close');
      });
    });
  
    it('CreateBillOfLading component should be loadable',() => {
      page.navigateTo('/CreateBillOfLading');
      browser.findElement(by.id('transactionName'))
      .then((transactionName) => {
        return transactionName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('CreateBillOfLading');
      });
    });
  
    it('CreateRiceExportCertificate component should be loadable',() => {
      page.navigateTo('/CreateRiceExportCertificate');
      browser.findElement(by.id('transactionName'))
      .then((transactionName) => {
        return transactionName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('CreateRiceExportCertificate');
      });
    });
  
    it('CreateDemoParticipants component should be loadable',() => {
      page.navigateTo('/CreateDemoParticipants');
      browser.findElement(by.id('transactionName'))
      .then((transactionName) => {
        return transactionName.getText();
      })
      .then((txt) => {
        expect(txt).toBe('CreateDemoParticipants');
      });
    });
  

});