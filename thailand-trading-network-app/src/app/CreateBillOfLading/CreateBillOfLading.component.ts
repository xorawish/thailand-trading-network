/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { CreateBillOfLadingService } from './CreateBillOfLading.service';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-createbilloflading',
  templateUrl: './CreateBillOfLading.component.html',
  styleUrls: ['./CreateBillOfLading.component.css'],
  providers: [CreateBillOfLadingService]
})
export class CreateBillOfLadingComponent implements OnInit {

  myForm: FormGroup;

  private allTransactions;
  private Transaction;
  private currentId;
  private errorMessage;

  shipper = new FormControl('', Validators.required);
  consignee = new FormControl('', Validators.required);
  notifyParty = new FormControl('', Validators.required);
  vessel = new FormControl('', Validators.required);
  portOfLoading = new FormControl('', Validators.required);
  placeOfAcceptance = new FormControl('', Validators.required);
  numberOfOriginalBillOfLading = new FormControl('', Validators.required);
  portOfDischarge = new FormControl('', Validators.required);
  finalDestination = new FormControl('', Validators.required);
  placeOfDelivery = new FormControl('', Validators.required);
  freightAndChargesPayableAt = new FormControl('', Validators.required);
  marksAndNumbers = new FormControl('', Validators.required);
  numberAndTypeOfPackages = new FormControl('', Validators.required);
  descriptionOfGoods = new FormControl('', Validators.required);
  weightKgs = new FormControl('', Validators.required);
  measurementM3 = new FormControl('', Validators.required);
  containerNos = new FormControl('', Validators.required);
  typeOfService = new FormControl('', Validators.required);
  deliveryAgent = new FormControl('', Validators.required);
  shipDate = new FormControl('', Validators.required);
  transactionId = new FormControl('', Validators.required);
  timestamp = new FormControl('', Validators.required);


  constructor(private serviceCreateBillOfLading: CreateBillOfLadingService, fb: FormBuilder) {
    this.myForm = fb.group({
      shipper: this.shipper,
      consignee: this.consignee,
      notifyParty: this.notifyParty,
      vessel: this.vessel,
      portOfLoading: this.portOfLoading,
      placeOfAcceptance: this.placeOfAcceptance,
      numberOfOriginalBillOfLading: this.numberOfOriginalBillOfLading,
      portOfDischarge: this.portOfDischarge,
      finalDestination: this.finalDestination,
      placeOfDelivery: this.placeOfDelivery,
      freightAndChargesPayableAt: this.freightAndChargesPayableAt,
      marksAndNumbers: this.marksAndNumbers,
      numberAndTypeOfPackages: this.numberAndTypeOfPackages,
      descriptionOfGoods: this.descriptionOfGoods,
      weightKgs: this.weightKgs,
      measurementM3: this.measurementM3,
      containerNos: this.containerNos,
      typeOfService: this.typeOfService,
      deliveryAgent: this.deliveryAgent,
      shipDate: this.shipDate,
      transactionId: this.transactionId,
      timestamp: this.timestamp
    });
  };

  ngOnInit(): void {
    this.loadAll();
  }

  loadAll(): Promise<any> {
    const tempList = [];
    return this.serviceCreateBillOfLading.getAll()
    .toPromise()
    .then((result) => {
      this.errorMessage = null;
      result.forEach(transaction => {
        tempList.push(transaction);
      });
      this.allTransactions = tempList;
    })
    .catch((error) => {
      if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else if (error === '404 - Not Found') {
        this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
      } else {
        this.errorMessage = error;
      }
    });
  }

	/**
   * Event handler for changing the checked state of a checkbox (handles array enumeration values)
   * @param {String} name - the name of the transaction field to update
   * @param {any} value - the enumeration value for which to toggle the checked state
   */
  changeArrayValue(name: string, value: any): void {
    const index = this[name].value.indexOf(value);
    if (index === -1) {
      this[name].value.push(value);
    } else {
      this[name].value.splice(index, 1);
    }
  }

	/**
	 * Checkbox helper, determining whether an enumeration value should be selected or not (for array enumeration values
   * only). This is used for checkboxes in the transaction updateDialog.
   * @param {String} name - the name of the transaction field to check
   * @param {any} value - the enumeration value to check for
   * @return {Boolean} whether the specified transaction field contains the provided value
   */
  hasArrayValue(name: string, value: any): boolean {
    return this[name].value.indexOf(value) !== -1;
  }

  addTransaction(form: any): Promise<any> {
    this.Transaction = {
      $class: 'org.thailand.trade.carrier.CreateBillOfLading',
      'shipper': this.shipper.value,
      'consignee': this.consignee.value,
      'notifyParty': this.notifyParty.value,
      'vessel': this.vessel.value,
      'portOfLoading': this.portOfLoading.value,
      'placeOfAcceptance': this.placeOfAcceptance.value,
      'numberOfOriginalBillOfLading': this.numberOfOriginalBillOfLading.value,
      'portOfDischarge': this.portOfDischarge.value,
      'finalDestination': this.finalDestination.value,
      'placeOfDelivery': this.placeOfDelivery.value,
      'freightAndChargesPayableAt': this.freightAndChargesPayableAt.value,
      'marksAndNumbers': this.marksAndNumbers.value,
      'numberAndTypeOfPackages': this.numberAndTypeOfPackages.value,
      'descriptionOfGoods': this.descriptionOfGoods.value,
      'weightKgs': this.weightKgs.value,
      'measurementM3': this.measurementM3.value,
      'containerNos': this.containerNos.value,
      'typeOfService': this.typeOfService.value,
      'deliveryAgent': this.deliveryAgent.value,
      'shipDate': this.shipDate.value,
      'transactionId': this.transactionId.value,
      'timestamp': this.timestamp.value
    };

    this.myForm.setValue({
      'shipper': null,
      'consignee': null,
      'notifyParty': null,
      'vessel': null,
      'portOfLoading': null,
      'placeOfAcceptance': null,
      'numberOfOriginalBillOfLading': null,
      'portOfDischarge': null,
      'finalDestination': null,
      'placeOfDelivery': null,
      'freightAndChargesPayableAt': null,
      'marksAndNumbers': null,
      'numberAndTypeOfPackages': null,
      'descriptionOfGoods': null,
      'weightKgs': null,
      'measurementM3': null,
      'containerNos': null,
      'typeOfService': null,
      'deliveryAgent': null,
      'shipDate': null,
      'transactionId': null,
      'timestamp': null
    });

    return this.serviceCreateBillOfLading.addTransaction(this.Transaction)
    .toPromise()
    .then(() => {
      this.errorMessage = null;
      this.myForm.setValue({
        'shipper': null,
        'consignee': null,
        'notifyParty': null,
        'vessel': null,
        'portOfLoading': null,
        'placeOfAcceptance': null,
        'numberOfOriginalBillOfLading': null,
        'portOfDischarge': null,
        'finalDestination': null,
        'placeOfDelivery': null,
        'freightAndChargesPayableAt': null,
        'marksAndNumbers': null,
        'numberAndTypeOfPackages': null,
        'descriptionOfGoods': null,
        'weightKgs': null,
        'measurementM3': null,
        'containerNos': null,
        'typeOfService': null,
        'deliveryAgent': null,
        'shipDate': null,
        'transactionId': null,
        'timestamp': null
      });
    })
    .catch((error) => {
      if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else {
        this.errorMessage = error;
      }
    });
  }

  updateTransaction(form: any): Promise<any> {
    this.Transaction = {
      $class: 'org.thailand.trade.carrier.CreateBillOfLading',
      'shipper': this.shipper.value,
      'consignee': this.consignee.value,
      'notifyParty': this.notifyParty.value,
      'vessel': this.vessel.value,
      'portOfLoading': this.portOfLoading.value,
      'placeOfAcceptance': this.placeOfAcceptance.value,
      'numberOfOriginalBillOfLading': this.numberOfOriginalBillOfLading.value,
      'portOfDischarge': this.portOfDischarge.value,
      'finalDestination': this.finalDestination.value,
      'placeOfDelivery': this.placeOfDelivery.value,
      'freightAndChargesPayableAt': this.freightAndChargesPayableAt.value,
      'marksAndNumbers': this.marksAndNumbers.value,
      'numberAndTypeOfPackages': this.numberAndTypeOfPackages.value,
      'descriptionOfGoods': this.descriptionOfGoods.value,
      'weightKgs': this.weightKgs.value,
      'measurementM3': this.measurementM3.value,
      'containerNos': this.containerNos.value,
      'typeOfService': this.typeOfService.value,
      'deliveryAgent': this.deliveryAgent.value,
      'shipDate': this.shipDate.value,
      'timestamp': this.timestamp.value
    };

    return this.serviceCreateBillOfLading.updateTransaction(form.get('transactionId').value, this.Transaction)
    .toPromise()
    .then(() => {
      this.errorMessage = null;
    })
    .catch((error) => {
      if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else if (error === '404 - Not Found') {
      this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
      } else {
        this.errorMessage = error;
      }
    });
  }

  deleteTransaction(): Promise<any> {

    return this.serviceCreateBillOfLading.deleteTransaction(this.currentId)
    .toPromise()
    .then(() => {
      this.errorMessage = null;
    })
    .catch((error) => {
      if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else if (error === '404 - Not Found') {
        this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
      } else {
        this.errorMessage = error;
      }
    });
  }

  setId(id: any): void {
    this.currentId = id;
  }

  getForm(id: any): Promise<any> {

    return this.serviceCreateBillOfLading.getTransaction(id)
    .toPromise()
    .then((result) => {
      this.errorMessage = null;
      const formObject = {
        'shipper': null,
        'consignee': null,
        'notifyParty': null,
        'vessel': null,
        'portOfLoading': null,
        'placeOfAcceptance': null,
        'numberOfOriginalBillOfLading': null,
        'portOfDischarge': null,
        'finalDestination': null,
        'placeOfDelivery': null,
        'freightAndChargesPayableAt': null,
        'marksAndNumbers': null,
        'numberAndTypeOfPackages': null,
        'descriptionOfGoods': null,
        'weightKgs': null,
        'measurementM3': null,
        'containerNos': null,
        'typeOfService': null,
        'deliveryAgent': null,
        'shipDate': null,
        'transactionId': null,
        'timestamp': null
      };

      if (result.shipper) {
        formObject.shipper = result.shipper;
      } else {
        formObject.shipper = null;
      }

      if (result.consignee) {
        formObject.consignee = result.consignee;
      } else {
        formObject.consignee = null;
      }

      if (result.notifyParty) {
        formObject.notifyParty = result.notifyParty;
      } else {
        formObject.notifyParty = null;
      }

      if (result.vessel) {
        formObject.vessel = result.vessel;
      } else {
        formObject.vessel = null;
      }

      if (result.portOfLoading) {
        formObject.portOfLoading = result.portOfLoading;
      } else {
        formObject.portOfLoading = null;
      }

      if (result.placeOfAcceptance) {
        formObject.placeOfAcceptance = result.placeOfAcceptance;
      } else {
        formObject.placeOfAcceptance = null;
      }

      if (result.numberOfOriginalBillOfLading) {
        formObject.numberOfOriginalBillOfLading = result.numberOfOriginalBillOfLading;
      } else {
        formObject.numberOfOriginalBillOfLading = null;
      }

      if (result.portOfDischarge) {
        formObject.portOfDischarge = result.portOfDischarge;
      } else {
        formObject.portOfDischarge = null;
      }

      if (result.finalDestination) {
        formObject.finalDestination = result.finalDestination;
      } else {
        formObject.finalDestination = null;
      }

      if (result.placeOfDelivery) {
        formObject.placeOfDelivery = result.placeOfDelivery;
      } else {
        formObject.placeOfDelivery = null;
      }

      if (result.freightAndChargesPayableAt) {
        formObject.freightAndChargesPayableAt = result.freightAndChargesPayableAt;
      } else {
        formObject.freightAndChargesPayableAt = null;
      }

      if (result.marksAndNumbers) {
        formObject.marksAndNumbers = result.marksAndNumbers;
      } else {
        formObject.marksAndNumbers = null;
      }

      if (result.numberAndTypeOfPackages) {
        formObject.numberAndTypeOfPackages = result.numberAndTypeOfPackages;
      } else {
        formObject.numberAndTypeOfPackages = null;
      }

      if (result.descriptionOfGoods) {
        formObject.descriptionOfGoods = result.descriptionOfGoods;
      } else {
        formObject.descriptionOfGoods = null;
      }

      if (result.weightKgs) {
        formObject.weightKgs = result.weightKgs;
      } else {
        formObject.weightKgs = null;
      }

      if (result.measurementM3) {
        formObject.measurementM3 = result.measurementM3;
      } else {
        formObject.measurementM3 = null;
      }

      if (result.containerNos) {
        formObject.containerNos = result.containerNos;
      } else {
        formObject.containerNos = null;
      }

      if (result.typeOfService) {
        formObject.typeOfService = result.typeOfService;
      } else {
        formObject.typeOfService = null;
      }

      if (result.deliveryAgent) {
        formObject.deliveryAgent = result.deliveryAgent;
      } else {
        formObject.deliveryAgent = null;
      }

      if (result.shipDate) {
        formObject.shipDate = result.shipDate;
      } else {
        formObject.shipDate = null;
      }

      if (result.transactionId) {
        formObject.transactionId = result.transactionId;
      } else {
        formObject.transactionId = null;
      }

      if (result.timestamp) {
        formObject.timestamp = result.timestamp;
      } else {
        formObject.timestamp = null;
      }

      this.myForm.setValue(formObject);

    })
    .catch((error) => {
      if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else if (error === '404 - Not Found') {
      this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
      } else {
        this.errorMessage = error;
      }
    });
  }

  resetForm(): void {
    this.myForm.setValue({
      'shipper': null,
      'consignee': null,
      'notifyParty': null,
      'vessel': null,
      'portOfLoading': null,
      'placeOfAcceptance': null,
      'numberOfOriginalBillOfLading': null,
      'portOfDischarge': null,
      'finalDestination': null,
      'placeOfDelivery': null,
      'freightAndChargesPayableAt': null,
      'marksAndNumbers': null,
      'numberAndTypeOfPackages': null,
      'descriptionOfGoods': null,
      'weightKgs': null,
      'measurementM3': null,
      'containerNos': null,
      'typeOfService': null,
      'deliveryAgent': null,
      'shipDate': null,
      'transactionId': null,
      'timestamp': null
    });
  }
}
