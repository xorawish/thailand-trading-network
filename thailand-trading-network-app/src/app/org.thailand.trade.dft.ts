import {Asset} from './org.hyperledger.composer.system';
import {Participant} from './org.hyperledger.composer.system';
import {Transaction} from './org.hyperledger.composer.system';
import {Event} from './org.hyperledger.composer.system';
import {Person} from './org.thailand.trade.participants';
// export namespace org.thailand.trade.dft{
   export class RiceExportCertificate extends Asset {
      certificateId: string;
      person: Person;
      companyName: string;
      contact: string;
      address: string;
      riceType: string;
      riceWeight: string;
   }
   export class CreateRiceExportCertificate extends Transaction {
      person: Person;
      companyName: string;
      contact: string;
      address: string;
      riceType: string;
      riceWeight: string;
   }
   export class RiceExportCertificateCreated extends Event {
      certificateId: string;
   }
// }
