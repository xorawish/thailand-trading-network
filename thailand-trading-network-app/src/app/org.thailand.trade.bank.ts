import {Asset} from './org.hyperledger.composer.system';
import {Participant} from './org.hyperledger.composer.system';
import {Transaction} from './org.hyperledger.composer.system';
import {Event} from './org.hyperledger.composer.system';
import {Customer,Bank,Person} from './org.thailand.trade.participants';
// export namespace org.thailand.trade.bank{
   export enum LetterStatus {
      AWAITING_APPROVAL,
      APPROVED,
      SHIPPED,
      RECEIVED,
      READY_FOR_PAYMENT,
      CLOSED,
      REJECTED,
   }
   export class LetterOfCredit extends Asset {
      letterId: string;
      applicant: Customer;
      beneficiary: Customer;
      issuingBank: Bank;
      exportingBank: Bank;
      rules: Rule[];
      productDetails: ProductDetails;
      evidence: string[];
      approval: Person[];
      status: LetterStatus;
      closeReason: string;
   }
   export class Rule {
      ruleId: string;
      ruleText: string;
   }
   export class ProductDetails {
      productType: string;
      quantity: number;
      pricePerUnit: number;
   }
   export class InitialApplication extends Transaction {
      letterId: string;
      applicant: Customer;
      beneficiary: Customer;
      rules: Rule[];
      productDetails: ProductDetails;
   }
   export class InitialApplicationEvent extends Event {
      loc: LetterOfCredit;
   }
   export class Approve extends Transaction {
      loc: LetterOfCredit;
      approvingParty: Person;
   }
   export class ApproveEvent extends Event {
      loc: LetterOfCredit;
      approvingParty: Person;
   }
   export class Reject extends Transaction {
      loc: LetterOfCredit;
      closeReason: string;
   }
   export class RejectEvent extends Event {
      loc: LetterOfCredit;
      closeReason: string;
   }
   export class SuggestChanges extends Transaction {
      loc: LetterOfCredit;
      rules: Rule[];
      suggestingParty: Person;
   }
   export class SuggestChangesEvent extends Event {
      loc: LetterOfCredit;
      rules: Rule[];
      suggestingParty: Person;
   }
   export class ShipProduct extends Transaction {
      loc: LetterOfCredit;
      evidence: string;
   }
   export class ShipProductEvent extends Event {
      loc: LetterOfCredit;
   }
   export class ReceiveProduct extends Transaction {
      loc: LetterOfCredit;
   }
   export class ReceiveProductEvent extends Event {
      loc: LetterOfCredit;
   }
   export class ReadyForPayment extends Transaction {
      loc: LetterOfCredit;
   }
   export class ReadyForPaymentEvent extends Event {
      loc: LetterOfCredit;
   }
   export class Close extends Transaction {
      loc: LetterOfCredit;
      closeReason: string;
   }
   export class CloseEvent extends Event {
      loc: LetterOfCredit;
      closeReason: string;
   }
// }
