/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';

import { LetterOfCreditComponent } from './LetterOfCredit/LetterOfCredit.component';
import { BillOfLadingComponent } from './BillOfLading/BillOfLading.component';
import { RiceExportCertificateComponent } from './RiceExportCertificate/RiceExportCertificate.component';

import { CarrierComponent } from './Carrier/Carrier.component';
import { BankComponent } from './Bank/Bank.component';
import { CustomerComponent } from './Customer/Customer.component';
import { BankEmployeeComponent } from './BankEmployee/BankEmployee.component';

import { InitialApplicationComponent } from './InitialApplication/InitialApplication.component';
import { ApproveComponent } from './Approve/Approve.component';
import { RejectComponent } from './Reject/Reject.component';
import { SuggestChangesComponent } from './SuggestChanges/SuggestChanges.component';
import { ShipProductComponent } from './ShipProduct/ShipProduct.component';
import { ReceiveProductComponent } from './ReceiveProduct/ReceiveProduct.component';
import { ReadyForPaymentComponent } from './ReadyForPayment/ReadyForPayment.component';
import { CloseComponent } from './Close/Close.component';
import { CreateBillOfLadingComponent } from './CreateBillOfLading/CreateBillOfLading.component';
import { CreateRiceExportCertificateComponent } from './CreateRiceExportCertificate/CreateRiceExportCertificate.component';
import { CreateDemoParticipantsComponent } from './CreateDemoParticipants/CreateDemoParticipants.component';
import { InfoComponent } from './info/info.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'LetterOfCredit', component: LetterOfCreditComponent },
  { path: 'BillOfLading', component: BillOfLadingComponent },
  { path: 'RiceExportCertificate', component: RiceExportCertificateComponent },
  { path: 'Carrier', component: CarrierComponent },
  { path: 'Bank', component: BankComponent },
  { path: 'Customer', component: CustomerComponent },
  { path: 'BankEmployee', component: BankEmployeeComponent },
  { path: 'InitialApplication', component: InitialApplicationComponent },
  { path: 'Approve', component: ApproveComponent },
  { path: 'Reject', component: RejectComponent },
  { path: 'SuggestChanges', component: SuggestChangesComponent },
  { path: 'ShipProduct', component: ShipProductComponent },
  { path: 'ReceiveProduct', component: ReceiveProductComponent },
  { path: 'ReadyForPayment', component: ReadyForPaymentComponent },
  { path: 'Close', component: CloseComponent },
  { path: 'CreateBillOfLading', component: CreateBillOfLadingComponent },
  { path: 'CreateRiceExportCertificate', component: CreateRiceExportCertificateComponent },
  { path: 'CreateDemoParticipants', component: CreateDemoParticipantsComponent },
  { path: 'Info', component: InfoComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
 imports: [RouterModule.forRoot(routes)],
 exports: [RouterModule],
 providers: []
})
export class AppRoutingModule { }
