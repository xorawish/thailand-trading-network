import {Asset} from './org.hyperledger.composer.system';
import {Participant} from './org.hyperledger.composer.system';
import {Transaction} from './org.hyperledger.composer.system';
import {Event} from './org.hyperledger.composer.system';
// export namespace org.thailand.trade.participants{
   export class Carrier extends Participant {
      carrierId: string;
   }
   export class Bank extends Participant {
      bankID: string;
      name: string;
   }
   export abstract class Person extends Participant {
      personId: string;
      name: string;
      lastName: string;
      bank: Bank;
   }
   export class Customer extends Person {
      companyName: string;
   }
   export class BankEmployee extends Person {
   }
   export class CreateDemoParticipants extends Transaction {
   }
// }
