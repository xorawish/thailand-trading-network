/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { LetterOfCreditService } from './LetterOfCredit.service';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-letterofcredit',
  templateUrl: './LetterOfCredit.component.html',
  styleUrls: ['./LetterOfCredit.component.css'],
  providers: [LetterOfCreditService]
})
export class LetterOfCreditComponent implements OnInit {

  myForm: FormGroup;

  private allAssets;
  private asset;
  private currentId;
  private errorMessage;

  letterId = new FormControl('', Validators.required);
  applicant = new FormControl('', Validators.required);
  beneficiary = new FormControl('', Validators.required);
  issuingBank = new FormControl('', Validators.required);
  exportingBank = new FormControl('', Validators.required);
  rules = new FormControl('', Validators.required);
  productDetails = new FormControl('', Validators.required);
  evidence = new FormControl('', Validators.required);
  approval = new FormControl('', Validators.required);
  status = new FormControl('', Validators.required);
  closeReason = new FormControl('', Validators.required);

  constructor(public serviceLetterOfCredit: LetterOfCreditService, fb: FormBuilder) {
    this.myForm = fb.group({
      letterId: this.letterId,
      applicant: this.applicant,
      beneficiary: this.beneficiary,
      issuingBank: this.issuingBank,
      exportingBank: this.exportingBank,
      rules: this.rules,
      productDetails: this.productDetails,
      evidence: this.evidence,
      approval: this.approval,
      status: this.status,
      closeReason: this.closeReason
    });
  };

  ngOnInit(): void {
    this.loadAll();
  }

  loadAll(): Promise<any> {
    const tempList = [];
    return this.serviceLetterOfCredit.getAll()
    .toPromise()
    .then((result) => {
      this.errorMessage = null;
      result.forEach(asset => {
        tempList.push(asset);
      });
      this.allAssets = tempList;
    })
    .catch((error) => {
      if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else if (error === '404 - Not Found') {
        this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
      } else {
        this.errorMessage = error;
      }
    });
  }

	/**
   * Event handler for changing the checked state of a checkbox (handles array enumeration values)
   * @param {String} name - the name of the asset field to update
   * @param {any} value - the enumeration value for which to toggle the checked state
   */
  changeArrayValue(name: string, value: any): void {
    const index = this[name].value.indexOf(value);
    if (index === -1) {
      this[name].value.push(value);
    } else {
      this[name].value.splice(index, 1);
    }
  }

	/**
	 * Checkbox helper, determining whether an enumeration value should be selected or not (for array enumeration values
   * only). This is used for checkboxes in the asset updateDialog.
   * @param {String} name - the name of the asset field to check
   * @param {any} value - the enumeration value to check for
   * @return {Boolean} whether the specified asset field contains the provided value
   */
  hasArrayValue(name: string, value: any): boolean {
    return this[name].value.indexOf(value) !== -1;
  }

  addAsset(form: any): Promise<any> {
    this.asset = {
      $class: 'org.thailand.trade.bank.LetterOfCredit',
      'letterId': this.letterId.value,
      'applicant': this.applicant.value,
      'beneficiary': this.beneficiary.value,
      'issuingBank': this.issuingBank.value,
      'exportingBank': this.exportingBank.value,
      'rules': this.rules.value,
      'productDetails': this.productDetails.value,
      'evidence': this.evidence.value,
      'approval': this.approval.value,
      'status': this.status.value,
      'closeReason': this.closeReason.value
    };

    this.myForm.setValue({
      'letterId': null,
      'applicant': null,
      'beneficiary': null,
      'issuingBank': null,
      'exportingBank': null,
      'rules': null,
      'productDetails': null,
      'evidence': null,
      'approval': null,
      'status': null,
      'closeReason': null
    });

    return this.serviceLetterOfCredit.addAsset(this.asset)
    .toPromise()
    .then(() => {
      this.errorMessage = null;
      this.myForm.setValue({
        'letterId': null,
        'applicant': null,
        'beneficiary': null,
        'issuingBank': null,
        'exportingBank': null,
        'rules': null,
        'productDetails': null,
        'evidence': null,
        'approval': null,
        'status': null,
        'closeReason': null
      });
      this.loadAll();
    })
    .catch((error) => {
      if (error === 'Server error') {
          this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else {
          this.errorMessage = error;
      }
    });
  }


  updateAsset(form: any): Promise<any> {
    this.asset = {
      $class: 'org.thailand.trade.bank.LetterOfCredit',
      'applicant': this.applicant.value,
      'beneficiary': this.beneficiary.value,
      'issuingBank': this.issuingBank.value,
      'exportingBank': this.exportingBank.value,
      'rules': this.rules.value,
      'productDetails': this.productDetails.value,
      'evidence': this.evidence.value,
      'approval': this.approval.value,
      'status': this.status.value,
      'closeReason': this.closeReason.value
    };

    return this.serviceLetterOfCredit.updateAsset(form.get('letterId').value, this.asset)
    .toPromise()
    .then(() => {
      this.errorMessage = null;
      this.loadAll();
    })
    .catch((error) => {
      if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else if (error === '404 - Not Found') {
        this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
      } else {
        this.errorMessage = error;
      }
    });
  }


  deleteAsset(): Promise<any> {

    return this.serviceLetterOfCredit.deleteAsset(this.currentId)
    .toPromise()
    .then(() => {
      this.errorMessage = null;
      this.loadAll();
    })
    .catch((error) => {
      if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else if (error === '404 - Not Found') {
        this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
      } else {
        this.errorMessage = error;
      }
    });
  }

  setId(id: any): void {
    this.currentId = id;
  }

  getForm(id: any): Promise<any> {

    return this.serviceLetterOfCredit.getAsset(id)
    .toPromise()
    .then((result) => {
      this.errorMessage = null;
      const formObject = {
        'letterId': null,
        'applicant': null,
        'beneficiary': null,
        'issuingBank': null,
        'exportingBank': null,
        'rules': null,
        'productDetails': null,
        'evidence': null,
        'approval': null,
        'status': null,
        'closeReason': null
      };

      if (result.letterId) {
        formObject.letterId = result.letterId;
      } else {
        formObject.letterId = null;
      }

      if (result.applicant) {
        formObject.applicant = result.applicant;
      } else {
        formObject.applicant = null;
      }

      if (result.beneficiary) {
        formObject.beneficiary = result.beneficiary;
      } else {
        formObject.beneficiary = null;
      }

      if (result.issuingBank) {
        formObject.issuingBank = result.issuingBank;
      } else {
        formObject.issuingBank = null;
      }

      if (result.exportingBank) {
        formObject.exportingBank = result.exportingBank;
      } else {
        formObject.exportingBank = null;
      }

      if (result.rules) {
        formObject.rules = result.rules;
      } else {
        formObject.rules = null;
      }

      if (result.productDetails) {
        formObject.productDetails = result.productDetails;
      } else {
        formObject.productDetails = null;
      }

      if (result.evidence) {
        formObject.evidence = result.evidence;
      } else {
        formObject.evidence = null;
      }

      if (result.approval) {
        formObject.approval = result.approval;
      } else {
        formObject.approval = null;
      }

      if (result.status) {
        formObject.status = result.status;
      } else {
        formObject.status = null;
      }

      if (result.closeReason) {
        formObject.closeReason = result.closeReason;
      } else {
        formObject.closeReason = null;
      }

      this.myForm.setValue(formObject);

    })
    .catch((error) => {
      if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else if (error === '404 - Not Found') {
        this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
      } else {
        this.errorMessage = error;
      }
    });
  }

  resetForm(): void {
    this.myForm.setValue({
      'letterId': null,
      'applicant': null,
      'beneficiary': null,
      'issuingBank': null,
      'exportingBank': null,
      'rules': null,
      'productDetails': null,
      'evidence': null,
      'approval': null,
      'status': null,
      'closeReason': null
      });
  }

}
