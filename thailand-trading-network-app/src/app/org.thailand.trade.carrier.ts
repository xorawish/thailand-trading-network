import {Asset} from './org.hyperledger.composer.system';
import {Participant} from './org.hyperledger.composer.system';
import {Transaction} from './org.hyperledger.composer.system';
import {Event} from './org.hyperledger.composer.system';
import {Person} from './org.thailand.trade.participants';
// export namespace org.thailand.trade.carrier{
   export class BillOfLading extends Asset {
      billOfLadingNo: string;
      shipper: Person;
      consignee: Person;
      notifyParty: string;
      vessel: string;
      portOfLoading: string;
      placeOfAcceptance: string;
      numberOfOriginalBillOfLading: number;
      portOfDischarge: string;
      finalDestination: string;
      placeOfDelivery: string;
      freightAndChargesPayableAt: string;
      marksAndNumbers: string;
      numberAndTypeOfPackages: string;
      descriptionOfGoods: string;
      weightKgs: number;
      measurementM3: number;
      containerNos: string[];
      typeOfService: string;
      deliveryAgent: string;
      shipDate: string;
   }
   export class CreateBillOfLading extends Transaction {
      shipper: Person;
      consignee: Person;
      notifyParty: string;
      vessel: string;
      portOfLoading: string;
      placeOfAcceptance: string;
      numberOfOriginalBillOfLading: number;
      portOfDischarge: string;
      finalDestination: string;
      placeOfDelivery: string;
      freightAndChargesPayableAt: string;
      marksAndNumbers: string;
      numberAndTypeOfPackages: string;
      descriptionOfGoods: string;
      weightKgs: number;
      measurementM3: number;
      containerNos: string[];
      typeOfService: string;
      deliveryAgent: string;
      shipDate: string;
   }
   export class BillOfLadingCreated extends Event {
      billOfLadingNo: string;
   }
// }
