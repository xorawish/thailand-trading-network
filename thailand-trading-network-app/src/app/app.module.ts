/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { DataService } from './data.service';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

import { LetterOfCreditComponent } from './LetterOfCredit/LetterOfCredit.component';
import { BillOfLadingComponent } from './BillOfLading/BillOfLading.component';
import { RiceExportCertificateComponent } from './RiceExportCertificate/RiceExportCertificate.component';

import { CarrierComponent } from './Carrier/Carrier.component';
import { BankComponent } from './Bank/Bank.component';
import { CustomerComponent } from './Customer/Customer.component';
import { BankEmployeeComponent } from './BankEmployee/BankEmployee.component';

import { InitialApplicationComponent } from './InitialApplication/InitialApplication.component';
import { ApproveComponent } from './Approve/Approve.component';
import { RejectComponent } from './Reject/Reject.component';
import { SuggestChangesComponent } from './SuggestChanges/SuggestChanges.component';
import { ShipProductComponent } from './ShipProduct/ShipProduct.component';
import { ReceiveProductComponent } from './ReceiveProduct/ReceiveProduct.component';
import { ReadyForPaymentComponent } from './ReadyForPayment/ReadyForPayment.component';
import { CloseComponent } from './Close/Close.component';
import { CreateBillOfLadingComponent } from './CreateBillOfLading/CreateBillOfLading.component';
import { CreateRiceExportCertificateComponent } from './CreateRiceExportCertificate/CreateRiceExportCertificate.component';
import { CreateDemoParticipantsComponent } from './CreateDemoParticipants/CreateDemoParticipants.component';
import { InfoComponent } from './info/info.component';

  @NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LetterOfCreditComponent,
    BillOfLadingComponent,
    RiceExportCertificateComponent,
    CarrierComponent,
    BankComponent,
    CustomerComponent,
    BankEmployeeComponent,
    InitialApplicationComponent,
    ApproveComponent,
    RejectComponent,
    SuggestChangesComponent,
    ShipProductComponent,
    ReceiveProductComponent,
    ReadyForPaymentComponent,
    CloseComponent,
    CreateBillOfLadingComponent,
    CreateRiceExportCertificateComponent,
    CreateDemoParticipantsComponent,
    InfoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
