/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { RiceExportCertificateService } from './RiceExportCertificate.service';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-riceexportcertificate',
  templateUrl: './RiceExportCertificate.component.html',
  styleUrls: ['./RiceExportCertificate.component.css'],
  providers: [RiceExportCertificateService]
})
export class RiceExportCertificateComponent implements OnInit {

  myForm: FormGroup;

  private allAssets;
  private asset;
  private currentId;
  private errorMessage;

  certificateId = new FormControl('', Validators.required);
  person = new FormControl('', Validators.required);
  companyName = new FormControl('', Validators.required);
  contact = new FormControl('', Validators.required);
  address = new FormControl('', Validators.required);
  riceType = new FormControl('', Validators.required);
  riceWeight = new FormControl('', Validators.required);

  constructor(public serviceRiceExportCertificate: RiceExportCertificateService, fb: FormBuilder) {
    this.myForm = fb.group({
      certificateId: this.certificateId,
      person: this.person,
      companyName: this.companyName,
      contact: this.contact,
      address: this.address,
      riceType: this.riceType,
      riceWeight: this.riceWeight
    });
  };

  ngOnInit(): void {
    this.loadAll();
  }

  loadAll(): Promise<any> {
    const tempList = [];
    return this.serviceRiceExportCertificate.getAll()
    .toPromise()
    .then((result) => {
      this.errorMessage = null;
      result.forEach(asset => {
        tempList.push(asset);
      });
      this.allAssets = tempList;
    })
    .catch((error) => {
      if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else if (error === '404 - Not Found') {
        this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
      } else {
        this.errorMessage = error;
      }
    });
  }

	/**
   * Event handler for changing the checked state of a checkbox (handles array enumeration values)
   * @param {String} name - the name of the asset field to update
   * @param {any} value - the enumeration value for which to toggle the checked state
   */
  changeArrayValue(name: string, value: any): void {
    const index = this[name].value.indexOf(value);
    if (index === -1) {
      this[name].value.push(value);
    } else {
      this[name].value.splice(index, 1);
    }
  }

	/**
	 * Checkbox helper, determining whether an enumeration value should be selected or not (for array enumeration values
   * only). This is used for checkboxes in the asset updateDialog.
   * @param {String} name - the name of the asset field to check
   * @param {any} value - the enumeration value to check for
   * @return {Boolean} whether the specified asset field contains the provided value
   */
  hasArrayValue(name: string, value: any): boolean {
    return this[name].value.indexOf(value) !== -1;
  }

  addAsset(form: any): Promise<any> {
    this.asset = {
      $class: 'org.thailand.trade.dft.RiceExportCertificate',
      'certificateId': this.certificateId.value,
      'person': this.person.value,
      'companyName': this.companyName.value,
      'contact': this.contact.value,
      'address': this.address.value,
      'riceType': this.riceType.value,
      'riceWeight': this.riceWeight.value
    };

    this.myForm.setValue({
      'certificateId': null,
      'person': null,
      'companyName': null,
      'contact': null,
      'address': null,
      'riceType': null,
      'riceWeight': null
    });

    return this.serviceRiceExportCertificate.addAsset(this.asset)
    .toPromise()
    .then(() => {
      this.errorMessage = null;
      this.myForm.setValue({
        'certificateId': null,
        'person': null,
        'companyName': null,
        'contact': null,
        'address': null,
        'riceType': null,
        'riceWeight': null
      });
      this.loadAll();
    })
    .catch((error) => {
      if (error === 'Server error') {
          this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else {
          this.errorMessage = error;
      }
    });
  }


  updateAsset(form: any): Promise<any> {
    this.asset = {
      $class: 'org.thailand.trade.dft.RiceExportCertificate',
      'person': this.person.value,
      'companyName': this.companyName.value,
      'contact': this.contact.value,
      'address': this.address.value,
      'riceType': this.riceType.value,
      'riceWeight': this.riceWeight.value
    };

    return this.serviceRiceExportCertificate.updateAsset(form.get('certificateId').value, this.asset)
    .toPromise()
    .then(() => {
      this.errorMessage = null;
      this.loadAll();
    })
    .catch((error) => {
      if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else if (error === '404 - Not Found') {
        this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
      } else {
        this.errorMessage = error;
      }
    });
  }


  deleteAsset(): Promise<any> {

    return this.serviceRiceExportCertificate.deleteAsset(this.currentId)
    .toPromise()
    .then(() => {
      this.errorMessage = null;
      this.loadAll();
    })
    .catch((error) => {
      if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else if (error === '404 - Not Found') {
        this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
      } else {
        this.errorMessage = error;
      }
    });
  }

  setId(id: any): void {
    this.currentId = id;
  }

  getForm(id: any): Promise<any> {

    return this.serviceRiceExportCertificate.getAsset(id)
    .toPromise()
    .then((result) => {
      this.errorMessage = null;
      const formObject = {
        'certificateId': null,
        'person': null,
        'companyName': null,
        'contact': null,
        'address': null,
        'riceType': null,
        'riceWeight': null
      };

      if (result.certificateId) {
        formObject.certificateId = result.certificateId;
      } else {
        formObject.certificateId = null;
      }

      if (result.person) {
        formObject.person = result.person;
      } else {
        formObject.person = null;
      }

      if (result.companyName) {
        formObject.companyName = result.companyName;
      } else {
        formObject.companyName = null;
      }

      if (result.contact) {
        formObject.contact = result.contact;
      } else {
        formObject.contact = null;
      }

      if (result.address) {
        formObject.address = result.address;
      } else {
        formObject.address = null;
      }

      if (result.riceType) {
        formObject.riceType = result.riceType;
      } else {
        formObject.riceType = null;
      }

      if (result.riceWeight) {
        formObject.riceWeight = result.riceWeight;
      } else {
        formObject.riceWeight = null;
      }

      this.myForm.setValue(formObject);

    })
    .catch((error) => {
      if (error === 'Server error') {
        this.errorMessage = 'Could not connect to REST server. Please check your configuration details';
      } else if (error === '404 - Not Found') {
        this.errorMessage = '404 - Could not find API route. Please check your available APIs.';
      } else {
        this.errorMessage = error;
      }
    });
  }

  resetForm(): void {
    this.myForm.setValue({
      'certificateId': null,
      'person': null,
      'companyName': null,
      'contact': null,
      'address': null,
      'riceType': null,
      'riceWeight': null
      });
  }

}
